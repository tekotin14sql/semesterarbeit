package ch.teko.vo;

public class BikeCategory {
	private final String name;

	public BikeCategory(
			String name
	) {
		this.name = name;
	}

	public String getName() { return this.name; }

}
