package ch.teko.vo;

import java.util.Date;

public class User {
	private final Integer id;
	private final String lastName;
	private final String firstName;
	private final String imageUrl;
	private final Date birtday;
	private final String address;
	private final BikeCategory category;

	public User(
			Integer id,
			String firstName,
			String lastName,
			String imageUrl,
			Date birtday,
			String address,
			BikeCategory category
	) {
		this.id = id;
		this.lastName = lastName;
		this.firstName = firstName;
		this.imageUrl = imageUrl;
		this.birtday = birtday;
		this.address = address;
		this.category = category;
	}

	public String getLastName() {
		return lastName;
	}

	public String getFirstName() {
		return firstName;
	}

	public String getFullname() { return this.firstName + " " + this.lastName; }

	public Integer getId() { return this.id; }

	public String getImage() { return this.imageUrl; }

	public String getCategory() { return this.category.getName(); }

	public String getBirtday() { return this.birtday.toString(); }

	public String getAddress() { return this.address; }

}
