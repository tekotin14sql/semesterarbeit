package ch.teko.vo;

import java.util.List;

public class Bike {
	private final Integer id;
	private final String name;
	private final String image_url;
	private final String mat_number;
	private final Double rent_price;
	private final Double price;
	private final String body_number;
	private final String specification;
	private final Status status;
	private final Vendor vendor;
	private final List<BikeCategory> bikeCategories;

	public Bike(
			Integer id,
			String name,
			String image_url,
			String mat_number,
			Double rent_price,
			Double price,
			String body_number,
			String specification,
			Status status,
			Vendor vendor,
			List<BikeCategory> bikeCategories
	) {
		this.id = id;
		this.name = name;
		this.image_url = image_url;
		this.mat_number = mat_number;
		this.rent_price = rent_price;
		this.price = price;
		this.body_number = body_number;
		this.specification = specification;
		this.status = status;
		this.vendor = vendor;
		this.bikeCategories = bikeCategories;
	}

	public void addBikeCategory(BikeCategory bikeCategory) {
		this.bikeCategories.add(bikeCategory);
	}

	public Integer getId() { return this.id; }

	public String getMatNumber() { return this.mat_number; }

	public String getFullname() { return this.vendor.getName() + ' ' + this.name; }

	public String getStatus() { return this.status.toString(); }

	public String getBodyNumber() { return this.body_number; }

	public String getVendor() { return this.vendor.getName(); }

	public String getRentPrice() { return this.rent_price + " CHF"; }

	public String getPrice() { return this.price + " CHF"; }

	public String getSpec() { return this.specification; }

	public String getCategoriesAsString() {
		String listString = "";
		for (BikeCategory bc : this.bikeCategories)
		{
			listString += bc.getName() + ", ";
		}
		return listString.substring(0, listString.length()-2);
	}

	public String getImage() { return this.image_url; }

}
