package ch.teko.vo;

public class Vendor {
	private final String name;

	public Vendor(
			String name
	) {
		this.name = name;
	}

	public String getName() { return this.name; }

}
