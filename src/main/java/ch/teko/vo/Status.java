package ch.teko.vo;

/**
 * Created by tzhweda9 on 25/08/16.
 */
public enum Status {
    INREPAIR,
    READY,
    INRENT,
    SOLD
}
