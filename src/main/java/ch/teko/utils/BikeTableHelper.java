package ch.teko.utils;

import ch.teko.vo.Bike;
import ch.teko.vo.Status;
import javafx.beans.property.SimpleStringProperty;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

/**
 * Created by tzhweda9 on 05/09/16.
 */
public class BikeTableHelper {

    public static void setupTableColumns(
            TableColumn<Bike, String> imageColumn,
            TableColumn<Bike, String> nameColumn,
            TableColumn<Bike, String> statusColumn,
            TableColumn<Bike, String> matNumberColumn
            ) {
        matNumberColumn.setCellValueFactory(bike -> new SimpleStringProperty(bike.getValue().getMatNumber()));
        nameColumn.setCellValueFactory(bike -> new SimpleStringProperty(bike.getValue().getFullname()));

        imageColumn.setCellValueFactory(bike -> new SimpleStringProperty(bike.getValue().getImage()));
        imageColumn.setCellFactory(column -> {
            return new TableCell<Bike, String>() {
                @Override
                protected void updateItem(String item, boolean empty) {
                    super.updateItem(item, empty);

                    if (item == null) {
                        setGraphic(null);
                    } else if (getGraphic() == null) {
                        ImageView img = new ImageView(new Image(item, true));
                        img.setFitHeight(20);
                        img.setFitWidth(30);
                        setGraphic(img);
                    }
                }
            };
        });

        statusColumn.setCellValueFactory(bike -> new SimpleStringProperty(bike.getValue().getStatus()));
        statusColumn.setCellFactory(column -> {
            return new TableCell<Bike, String>() {
                @Override
                protected void updateItem(String item, boolean empty) {
                    super.updateItem(item, empty);

                    setText(null);

                    if (item != null) {
                        Status status = Status.valueOf(item.toUpperCase());

                        switch (status) {
                            case INRENT:
                                setStyle("-fx-background-color: darkred");
                                break;
                            case INREPAIR:
                                setStyle("-fx-background-color: yellow");
                                break;
                            case SOLD:
                                setStyle("-fx-background-color: black");
                                break;
                            case READY:
                                setStyle("-fx-background-color: darkgreen");
                                break;
                        }
                    } else {
                        setStyle("-fx-background-color: transparent");
                    }
                }
            };
        });
    }

}
