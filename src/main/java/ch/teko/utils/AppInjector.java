package ch.teko.utils;

import ch.teko.dao.*;
import com.google.inject.AbstractModule;

public class AppInjector extends AbstractModule {

    @Override
    protected void configure() {
        bind(BikeDAO.class).to(MySqlBikeDAO.class);
        bind(UserDAO.class).to(MySqlUserDAO.class);
        bind(TranslationHelperDAO.class).to(PropertyFileTranslationHelperDAO.class);
    }

}
