package ch.teko.dao;

import ch.teko.vo.BikeCategory;
import ch.teko.vo.User;

import javax.inject.Singleton;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

@Singleton
public class MySqlUserDAO implements UserDAO {

	@Override
	public User getUserById(Integer id) {
		try (
				Connection conn = ConnectionManager.getConnection();
			 	PreparedStatement statement = conn.prepareStatement("select * from v_users where id = ?;")
		) {
			statement.setInt(1, id);
			ResultSet rs = statement.executeQuery();
			if (rs.next()) {
				return this.getUserFromResultSet(rs);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public List<User> getAllUsers() {
		List<User> userList = new ArrayList<>();
		try (
				Connection conn = ConnectionManager.getConnection();
				PreparedStatement statement = conn.prepareStatement(
						"select * from v_users;"
				)
		) {
			ResultSet rs = statement.executeQuery();
			while (rs.next()){
				userList.add(this.getUserFromResultSet(rs));
			};
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return userList;
	}

	@Override
	public List<User> getUserByFuzzy(String search) {
		List<User> userList = new ArrayList<>();
		try (
				Connection conn = ConnectionManager.getConnection();
				PreparedStatement statement = conn.prepareStatement(
						"select * from v_users where firstname LIKE ? OR lastname LIKE ? OR address LIKE ?;"
				)
		) {
			statement.setString(1, "%" + search + "%");
			statement.setString(2, "%" + search + "%");
			statement.setString(3, "%" + search + "%");
			ResultSet rs = statement.executeQuery();
			while (rs.next()) {
				userList.add(this.getUserFromResultSet(rs));
			};
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return userList;
	}

	private User getUserFromResultSet(ResultSet rs) throws SQLException {
		return new User(
			rs.getInt(1),
			rs.getString(2),
			rs.getString(3),
			rs.getString(4),
			rs.getDate(5),
			rs.getString(6),
			new BikeCategory(rs.getString(7))
		);
	}

}
