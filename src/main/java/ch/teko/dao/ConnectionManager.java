package ch.teko.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public final class ConnectionManager {
	private static ConnectionManager instance = new ConnectionManager();
	private static final String DB_URL = "jdbc:mysql://localhost:3306/bike_rent";
	private static final String USER = "bike_rent_app";
	private static final String PASS = "12345";

	private ConnectionManager() {
	}

	private Connection createConnection() {
		Connection conn = null;
		try {
			conn = DriverManager.getConnection(DB_URL, USER, PASS);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return conn;
	}

	public static Connection getConnection() {
		return instance.createConnection();
	}
}
