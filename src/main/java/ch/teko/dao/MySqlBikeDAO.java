package ch.teko.dao;

import ch.teko.vo.*;

import javax.inject.Singleton;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

@Singleton
public class MySqlBikeDAO implements BikeDAO {


    @Override
    public List<Bike> getAllBikes(Boolean rent) {
        List<Bike> bikeList = new ArrayList<>();

        String query = new String("select * from v_bikes");

        if (rent) {
            query += " WHERE status = \"inrent\"";
        }

        try (
                Connection conn = ConnectionManager.getConnection();
                PreparedStatement statement = conn.prepareStatement(
                        query + ";"
                )
        ) {
            ResultSet rs = statement.executeQuery();
            bikeList = this.getBikeList(rs);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return bikeList;
    }

    @Override
    public List<Bike> getAllBikes() {
        return this.getAllBikes(false);
    }

    @Override
    public List<Bike> getBikeHistory(User user) {
        List<Bike> bikeList = new ArrayList<>();
        try (
                Connection conn = ConnectionManager.getConnection();
                PreparedStatement statement = conn.prepareStatement(
                        "select * from v_user_bike_history where users_id = ?;"
                )
        ) {
            statement.setInt(1, user.getId());
            ResultSet rs = statement.executeQuery();
            bikeList = this.getBikeList(rs);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return bikeList;
    }

    @Override
    public Bike getBikeById(Bike bike) {
        try (
                Connection conn = ConnectionManager.getConnection();
                PreparedStatement statement = conn.prepareStatement(
                    "select * from v_bikes where id = ?;"
        )
        ) {
            statement.setInt(1, bike.getId());
            ResultSet rs = statement.executeQuery();
            if (rs.next()) {
                return this.getBikeFromResultSet(rs);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public Bike sellBike(Bike bike) {
        try (Connection conn = ConnectionManager.getConnection();
             PreparedStatement statement = conn.prepareStatement(
                     "update bikes set bike_status_id = 4 where id = ?;")
        ) {
            statement.setInt(1, bike.getId());
            statement.executeUpdate();
            return this.getBikeById(bike);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public Bike repairBike(Bike bike) {
        try (Connection conn = ConnectionManager.getConnection();
             PreparedStatement statement = conn.prepareStatement(
                     "update bikes set bike_status_id = 1 where id = ?;")
        ) {
            statement.setInt(1, bike.getId());
            statement.executeUpdate();
            return this.getBikeById(bike);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public Bike readyBike(Bike bike) {
        try (Connection conn = ConnectionManager.getConnection();
             PreparedStatement statement = conn.prepareStatement(
                     "update bikes set bike_status_id = 2 where id = ?;")
        ) {
            statement.setInt(1, bike.getId());
            statement.executeUpdate();
            return this.getBikeById(bike);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public Bike rentBike(Bike bike, User user, Date dateFrom, Date dateTo) {
        try (Connection conn = ConnectionManager.getConnection();
             PreparedStatement statement = conn.prepareStatement(
                     "insert into users_rents_bikes (users_id, bikes_id, date_from, date_to) values (?, ?, ?, ?);")
        ) {
            statement.setInt(1, user.getId());
            statement.setInt(2, bike.getId());
            statement.setDate(3, new java.sql.Date(dateFrom.getTime()));
            statement.setDate(4, new java.sql.Date(dateTo.getTime()));
            statement.executeUpdate();
            return this.getBikeById(bike);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public List<Bike> getBikeByFuzzy(String search, Boolean rent) {
        List<Bike> bikeList = new ArrayList<>();

        String query = new String("select * from v_bikes WHERE name LIKE ? OR mat_number LIKE ? OR body_number LIKE ? OR vendor LIKE ?");

        if (rent) {
            query += " AND status = \"inrent\"";
        }

        try (
                Connection conn = ConnectionManager.getConnection();
                PreparedStatement statement = conn.prepareStatement(
                        query + ";"
                )
        ) {
            statement.setString(1, "%" + search + "%");
            statement.setString(2, "%" + search + "%");
            statement.setString(3, "%" + search + "%");
            statement.setString(4, "%" + search + "%");
            ResultSet rs = statement.executeQuery();
            bikeList = this.getBikeList(rs);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return bikeList;
    }

    @Override
    public List<Bike> getBikeByFuzzy(String search) {
        return this.getBikeByFuzzy(search, false);
    }

    private List<Bike> getBikeList(ResultSet rs) throws SQLException {
        HashMap<Integer, Bike> bikeList = new HashMap<>();
        while (rs.next()) {
            Bike bike = bikeList.get(rs.getInt(1));
            if (bike == null) {
                bikeList.put(rs.getInt(1), this.getBikeFromResultSet(rs));
            } else {
                bike.addBikeCategory(new BikeCategory(rs.getString(10)));
            }
        }
        return new ArrayList<>(bikeList.values());
    }

    private Bike getBikeFromResultSet(ResultSet rs) throws SQLException {
        List<BikeCategory> bikeCategories = new ArrayList<>();
        bikeCategories.add(new BikeCategory(rs.getString(11)));
        return new Bike(
                rs.getInt(1),
                rs.getString(2),
                rs.getString(3),
                rs.getString(4),
                rs.getDouble(5),
                rs.getDouble(6),
                rs.getString(7),
                rs.getString(8),
                Status.valueOf(rs.getString(9).toUpperCase()),
                new Vendor(rs.getString(10)),
                bikeCategories
        );
    }
}
