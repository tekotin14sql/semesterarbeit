package ch.teko.dao;

import ch.teko.vo.Bike;
import ch.teko.vo.User;

import java.util.Date;
import java.util.List;

public interface BikeDAO {

	public List<Bike> getAllBikes(Boolean rent);
	public List<Bike> getAllBikes();

	public List<Bike> getBikeByFuzzy(String search, Boolean rent);
	public List<Bike> getBikeByFuzzy(String search);

	public List<Bike> getBikeHistory(User user);

	public Bike getBikeById(Bike bike);

	public Bike sellBike(Bike bike);

	public Bike repairBike(Bike bike);

	public Bike readyBike(Bike bike);

	public Bike rentBike(Bike bike, User user, Date dateFrom, Date dateTo);
}
