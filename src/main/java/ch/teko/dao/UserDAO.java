package ch.teko.dao;

import ch.teko.vo.User;

import java.util.List;

public interface UserDAO {

	public User getUserById(Integer id);

	public List<User> getAllUsers();

	public List<User> getUserByFuzzy(String search);
}
