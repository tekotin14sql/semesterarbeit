package ch.teko.dao;

import com.google.inject.Singleton;

import java.io.IOException;
import java.io.InputStream;
import java.util.PropertyResourceBundle;
import java.util.ResourceBundle;

@Singleton
public class PropertyFileTranslationHelperDAO implements TranslationHelperDAO {

    private ResourceBundle translationBundle = null;

    PropertyFileTranslationHelperDAO() {
        try {
            ClassLoader classLoader = Thread.currentThread().getContextClassLoader();
            InputStream inputStream = classLoader.getResource("translation_en.properties").openStream();
            this.translationBundle = new PropertyResourceBundle(inputStream);
        }
        catch (IOException e) {
            e.printStackTrace();
        }
    }

    public ResourceBundle getBundle() {
        return this.translationBundle;
    }

    public String getTranslation(String key) {
        return this.translationBundle.getString(key);
    }

}
