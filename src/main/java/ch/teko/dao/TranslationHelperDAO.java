package ch.teko.dao;

import java.util.ResourceBundle;

/**
 * Created by tzhweda9 on 05/09/16.
 */
public interface TranslationHelperDAO {

    public ResourceBundle getBundle();

    public String getTranslation(String key);
}
