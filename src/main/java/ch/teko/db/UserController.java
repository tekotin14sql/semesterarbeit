package ch.teko.db;

import ch.teko.dao.BikeDAO;
import ch.teko.utils.AppInjector;
import ch.teko.utils.BikeTableHelper;
import ch.teko.vo.Bike;
import ch.teko.vo.User;
import com.google.inject.Guice;
import com.google.inject.Injector;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.text.Text;

public class UserController {

	private User user;
	private BikeDAO bikeDAO;

	@FXML
	private Label fullname;
	@FXML
	private Label category;
	@FXML
	private Label birthday;
	@FXML
	private Label favCategory;
	@FXML
	private Text address;
	@FXML
	private ImageView userImage;
	@FXML
	private TableView<Bike> bikeHistory;
	@FXML
	private TableColumn<Bike, String> imageColumn;
	@FXML
	private TableColumn<Bike, String> nameColumn;
	@FXML
	private TableColumn<Bike, String> statusColumn;
	@FXML
	private TableColumn<Bike, String> matNumberColumn;

	public UserController(User user) {
		this.user = user;
		Injector injector = Guice.createInjector(new AppInjector());
		this.bikeDAO = injector.getInstance(BikeDAO.class);
	}

	public void initialize() {
		// Setup FX elements
		this.setupFxElements();

		// load bike history
		this.loadBikeHistory();
	}

	private void loadBikeHistory() {
		this.bikeHistory.getItems().clear();
		this.bikeHistory.getItems().addAll(this.bikeDAO.getBikeHistory(this.user));
	}

	private void createBikeTableColumns() {
		BikeTableHelper.setupTableColumns(this.imageColumn, this.nameColumn, this.statusColumn, this.matNumberColumn);
	}

	private void setupFxElements() {
		// Initialize all text labels
		this.fullname.textProperty().setValue(this.user.getFullname());
		this.category.textProperty().setValue(this.user.getCategory());
		this.birthday.textProperty().setValue(this.user.getBirtday());
		this.favCategory.textProperty().setValue(this.user.getCategory());
		this.address.textProperty().setValue(this.user.getAddress());

		// Load image
		Image img = new Image(user.getImage(), true);
		this.userImage.imageProperty().setValue(img);

		// setup table columns
		this.createBikeTableColumns();
	}
}
