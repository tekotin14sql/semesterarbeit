package ch.teko.db;

import ch.teko.dao.BikeDAO;
import ch.teko.dao.PropertyFileTranslationHelperDAO;
import ch.teko.dao.TranslationHelperDAO;
import ch.teko.utils.AppInjector;
import ch.teko.utils.BikeTableHelper;
import ch.teko.vo.Bike;
import com.google.inject.Guice;
import com.google.inject.Injector;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.control.*;

public class BikeSearchController {

    private BikeDAO bikeDAO;
    private TranslationHelperDAO translationHelper;

    @FXML
    private TextField searchField;
    @FXML
    private CheckBox rentFilter;
    @FXML
    private TableView<Bike> resultTable;
    @FXML
    private TableColumn<Bike, String> imageColumn;
    @FXML
    private TableColumn<Bike, String> nameColumn;
    @FXML
    private TableColumn<Bike, String> statusColumn;
    @FXML
    private TableColumn<Bike, String> matNumberColumn;

    public BikeSearchController() {
        Injector injector = Guice.createInjector(new AppInjector());
        this.bikeDAO = injector.getInstance(BikeDAO.class);
        this.translationHelper = injector.getInstance(PropertyFileTranslationHelperDAO.class);
    }

    public void initialize() {
        // Load all Bikes
        this.loadBikes();

        // Initialize table
        this.createBikeTableColumns();

        // Listen for search changes
        this.searchField.textProperty().addListener((observable, oldValue, newValue) -> {
            this.loadBikes();
        });
        this.rentFilter.selectedProperty().addListener((observable, oldValue, newValue) -> {
            this.loadBikes();
        });

        // Row double click
        this.resultTable.setRowFactory(tv -> {
            TableRow<Bike> row = new TableRow<>();
            row.setOnMouseClicked(event -> {
                if (event.getClickCount() == 2 && (!row.isEmpty())) {
                    Bike rowData = row.getItem();
                    this.showDialog(rowData);
                }
            });
            return row;
        });
    }

    private void loadBikes() {
        this.resultTable.getItems().clear();
        if (this.searchField.textProperty().getValue().isEmpty()) {
            this.resultTable.getItems().addAll(this.bikeDAO.getAllBikes(this.rentFilter.isSelected()));
        } else {
            this.resultTable.getItems().addAll(this.bikeDAO.getBikeByFuzzy(this.searchField.textProperty().getValue(), this.rentFilter.isSelected()));
        }
    }

    private void createBikeTableColumns() {
        BikeTableHelper.setupTableColumns(this.imageColumn, this.nameColumn, this.statusColumn, this.matNumberColumn);
    }

    private void showDialog(final Bike bike) {

        try {
            Dialog<ButtonType> dialog = new Dialog<>();
            FXMLLoader loader = new FXMLLoader(getClass().getClassLoader().getResource("bikeDetail.fxml"), this.translationHelper.getBundle());
            loader.setControllerFactory(t -> new BikeDetailController(bike));
            try {
                dialog.getDialogPane().setContent(loader.load());
            } catch (Exception e) {
                e.printStackTrace();
            }
            dialog.getDialogPane().getButtonTypes().addAll(ButtonType.CLOSE);
            Node closeButton = dialog.getDialogPane().lookupButton(ButtonType.CLOSE);
            closeButton.managedProperty().bind(closeButton.visibleProperty());
            dialog.showAndWait();
            this.loadBikes();
        }
        catch (Exception $e) {
            $e.printStackTrace();
        }
    }
}
