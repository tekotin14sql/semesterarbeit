package ch.teko.db;

import ch.teko.dao.PropertyFileTranslationHelperDAO;
import ch.teko.dao.TranslationHelperDAO;
import ch.teko.utils.AppInjector;
import com.google.inject.Guice;
import com.google.inject.Injector;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class Main extends Application {

	@Override
	public void start(Stage primaryStage) throws Exception {

		// Get translation
		Injector injector = Guice.createInjector(new AppInjector());
		TranslationHelperDAO translationHelper = injector.getInstance(PropertyFileTranslationHelperDAO.class);

		// Load first view
		FXMLLoader loader = new FXMLLoader(getClass().getClassLoader().getResource("bikeSearch.fxml"), translationHelper.getBundle());
		loader.setController(new BikeSearchController());

		// Initialize first scene
		primaryStage.setScene(new Scene(loader.load()));
		primaryStage.setTitle("Bike Rent");
		primaryStage.show();
	}

	public static void main(String[] args) throws Exception {
		launch(args);
	}

}
