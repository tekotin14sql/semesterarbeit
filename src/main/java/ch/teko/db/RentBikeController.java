package ch.teko.db;

import ch.teko.dao.BikeDAO;
import ch.teko.dao.PropertyFileTranslationHelperDAO;
import ch.teko.dao.TranslationHelperDAO;
import ch.teko.dao.UserDAO;
import ch.teko.utils.AppInjector;
import ch.teko.utils.Toast;
import ch.teko.vo.Bike;
import ch.teko.vo.User;
import com.google.inject.Guice;
import com.google.inject.Injector;
import javafx.beans.property.SimpleStringProperty;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.stage.Stage;

import java.io.InputStream;
import java.time.LocalDate;
import java.util.PropertyResourceBundle;
import java.util.ResourceBundle;

public class RentBikeController {

	private BikeDAO bikeDAO;
	private Bike bike;

	private UserDAO userDAO;
	private User user;

	private TranslationHelperDAO translationHelper;

	@FXML
	private Label bikeTitle;
	@FXML
	private Label matNumber;
	@FXML
	private DatePicker dateFrom;
	@FXML
	private DatePicker dateTo;
	@FXML
	private TextField searchField;
	@FXML
	private Button btnCancel;
	@FXML
	private Button btnSave;
	@FXML
	private TableView<User> userTable;
	@FXML
	private TableColumn<User, String> userImageColumn;
	@FXML
	private TableColumn<User, String> firstnameColumn;
	@FXML
	private TableColumn<User, String> lastnameColumn;

	public RentBikeController(Bike bike) {
		this.bike = bike;
		Injector injector = Guice.createInjector(new AppInjector());
		this.bikeDAO = injector.getInstance(BikeDAO.class);
		this.userDAO = injector.getInstance(UserDAO.class);
		this.translationHelper = injector.getInstance(PropertyFileTranslationHelperDAO.class);
	}

	public void initialize() {
		// Setup FX elements
		this.setupFxElements();
		this.createUserTableColumns();

		// Load users
		this.loadUsers();

		// Catch action
		this.btnSave.setOnAction((event) -> {
			// Validation and save rental entry
			Stage stage = (Stage) this.btnSave.getScene().getWindow();
			this.disableInteraction();
			LocalDate now = LocalDate.now();
			if (this.dateFrom.getValue() == null || this.dateFrom.getValue().isBefore(now)) {
				Toast.makeTextError(stage, this.translationHelper.getTranslation("bike.rent.toast_date_now"));
				this.enableInteraction();
				return;
			}
			if (this.dateTo.getValue() == null || this.dateFrom.getValue().isAfter(this.dateTo.getValue())) {
				Toast.makeTextError(stage, this.translationHelper.getTranslation("bike.rent.toast_date_end_smaller"));
				this.enableInteraction();
				return;
			}
			if (this.user == null) {
				Toast.makeTextError(stage, this.translationHelper.getTranslation("bike.rent.toast_user_missing"));
				this.enableInteraction();
				return;
			}
			java.sql.Date dateFrom = java.sql.Date.valueOf(this.dateFrom.getValue());
			java.sql.Date dateTo = java.sql.Date.valueOf(this.dateTo.getValue());
			this.bike = this.bikeDAO.rentBike(this.bike, this.user, dateFrom, dateTo);
			stage.close();
		});
		this.btnCancel.setOnAction((event) -> {
			// Cancel and close dialog
			Stage stage = (Stage) this.btnCancel.getScene().getWindow();
			stage.close();
		});
		this.searchField.textProperty().addListener((observable, oldValue, newValue) -> {
			// Filter users on input change
			this.loadUsers();
		});
		// Row double click handler
		this.userTable.setRowFactory(tv -> {
			TableRow<User> row = new TableRow<>();
			row.setOnMouseClicked(event -> {
				if (event.getClickCount() == 2 && (!row.isEmpty())) {
					this.user = row.getItem();
					this.showUserDialog();
				} else if (event.getClickCount() == 1 && (!row.isEmpty())) {
					this.user = row.getItem();
				}
			});
			return row;
		});
	}

	private void loadUsers() {
		this.userTable.getItems().clear();
		if (this.searchField.textProperty().getValue().isEmpty()) {
			this.userTable.getItems().addAll(this.userDAO.getAllUsers());
		} else {
			this.userTable.getItems().addAll(this.userDAO.getUserByFuzzy(this.searchField.textProperty().getValue()));
		}
	}

	private void showUserDialog() {

		try {
			ClassLoader classLoader = Thread.currentThread().getContextClassLoader();
			InputStream inputStream = classLoader.getResource("translation_en.properties").openStream();
			ResourceBundle bundle = new PropertyResourceBundle(inputStream);

			Dialog<ButtonType> dialog = new Dialog<>();
			FXMLLoader loader = new FXMLLoader(getClass().getClassLoader().getResource("user.fxml"), bundle);
			loader.setControllerFactory(t -> new UserController(this.user));
			try {
				dialog.getDialogPane().setContent(loader.load());
			} catch (Exception e) {
				e.printStackTrace();
			}
			dialog.getDialogPane().getButtonTypes().addAll(ButtonType.CLOSE);
			Node closeButton = dialog.getDialogPane().lookupButton(ButtonType.CLOSE);
			closeButton.managedProperty().bind(closeButton.visibleProperty());
			dialog.showAndWait();
		}
		catch (Exception $e) {
			$e.printStackTrace();
		}
	}

	private void createUserTableColumns() {
		this.firstnameColumn.setCellValueFactory(user -> new SimpleStringProperty(user.getValue().getFirstName()));
		this.lastnameColumn.setCellValueFactory(user -> new SimpleStringProperty(user.getValue().getLastName()));

		this.userImageColumn.setCellValueFactory(bike -> new SimpleStringProperty(bike.getValue().getImage()));
		this.userImageColumn.setCellFactory(column -> {
			return new TableCell<User, String>() {
				@Override
				protected void updateItem(String item, boolean empty) {
					super.updateItem(item, empty);

					if (item == null) {
						setGraphic(null);
					} else if (getGraphic() == null) {
						ImageView img = new ImageView(new Image(item, true));
						img.setFitHeight(15);
						img.setFitWidth(30);
						setGraphic(img);
					}
				}
			};
		});
	}

	private void disableInteraction() {
		this.btnSave.disableProperty().setValue(true);
		this.btnCancel.disableProperty().setValue(true);
	}

	private void enableInteraction() {
		this.btnSave.disableProperty().setValue(false);
		this.btnCancel.disableProperty().setValue(false);
	}

	private void setupFxElements() {
		// Initialize all text labels
		this.bikeTitle.textProperty().setValue(this.bike.getFullname());
		this.matNumber.textProperty().setValue(this.bike.getMatNumber());
	}
}
