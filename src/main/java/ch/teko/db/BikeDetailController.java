package ch.teko.db;

import ch.teko.dao.BikeDAO;
import ch.teko.dao.PropertyFileTranslationHelperDAO;
import ch.teko.dao.TranslationHelperDAO;
import ch.teko.utils.AppInjector;
import ch.teko.utils.Toast;
import ch.teko.vo.Bike;
import ch.teko.vo.Status;
import com.google.inject.Guice;
import com.google.inject.Injector;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Dialog;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.text.Text;
import javafx.stage.Stage;

public class BikeDetailController {

	private Bike bike;

	private BikeDAO bikeDAO;
	private TranslationHelperDAO translationHelper;

	@FXML
	private Label bikeTitle;
	@FXML
	private ImageView bikeImage;
	@FXML
	private Label matNumber;
	@FXML
	private Label vendor;
	@FXML
	private Label rentPrice;
	@FXML
	private Label price;
	@FXML
	private Label bodyNumber;
	@FXML
	private Label category;
	@FXML
	private Label status;
	@FXML
	private Text spec;
	@FXML
	private Button btnRent;
	@FXML
	private Button btnSell;
	@FXML
	private Button btnRepair;
	@FXML
	private Button btnReady;


	public BikeDetailController(Bike bike) {
		this.bike = bike;
		Injector injector = Guice.createInjector(new AppInjector());
		this.bikeDAO = injector.getInstance(BikeDAO.class);
		this.translationHelper = injector.getInstance(PropertyFileTranslationHelperDAO.class);
	}

	public void initialize() {
		// Setup FX elements
		this.setupFxElements();

		// Catch action
		this.btnSell.setOnAction((event) -> {
			// sell bike
			this.disableInteraction();
			this.bike = this.bikeDAO.sellBike(this.bike);
			this.setupFxElements();
			this.enableInteraction();
			Toast.makeText((Stage) this.btnSell.getScene().getWindow(), this.translationHelper.getTranslation("bike.detail.toast_sell"));
		});
		this.btnReady.setOnAction((event) -> {
			// Put bike back to ready
			this.disableInteraction();
			this.bike = this.bikeDAO.readyBike(this.bike);
			this.setupFxElements();
			this.enableInteraction();
			Toast.makeText((Stage) this.btnSell.getScene().getWindow(), this.translationHelper.getTranslation("bike.detail.toast_ready"));
		});
		this.btnRepair.setOnAction((event) -> {
			// Put bike in repair
			this.disableInteraction();
			this.bike = this.bikeDAO.repairBike(this.bike);
			this.setupFxElements();
			this.enableInteraction();
			Toast.makeText((Stage) this.btnSell.getScene().getWindow(), this.translationHelper.getTranslation("bike.detail.toast_repair"));
		});
		this.btnRent.setOnAction((event) -> {
			// show rent dialog
			this.showRentDialog();
		});
	}

	private void showRentDialog() {
		try {
			Dialog<ButtonType> dialog = new Dialog<>();
			FXMLLoader loader = new FXMLLoader(getClass().getClassLoader().getResource("rentBike.fxml"), this.translationHelper.getBundle());
			loader.setControllerFactory(t -> new RentBikeController(this.bike));
			try {
				dialog.getDialogPane().setContent(loader.load());
			} catch (Exception e) {
				e.printStackTrace();
			}
			dialog.showAndWait();
			this.bike = this.bikeDAO.getBikeById(bike);
			this.setupFxElements();
		}
		catch (Exception $e) {
			$e.printStackTrace();
		}
	}

	private void disableInteraction() {
		this.btnRent.disableProperty().setValue(true);
		this.btnSell.disableProperty().setValue(true);
		this.btnReady.disableProperty().setValue(true);
		this.btnRepair.disableProperty().setValue(true);
	}

	private void enableInteraction() {
		this.btnRent.disableProperty().setValue(false);
		this.btnSell.disableProperty().setValue(false);
		this.btnReady.disableProperty().setValue(false);
		this.btnRepair.disableProperty().setValue(false);
	}

	private void setupFxElements() {
		// Initialize all text labels
		this.bikeTitle.textProperty().setValue(this.bike.getFullname());
		this.matNumber.textProperty().setValue(this.bike.getMatNumber());
		this.vendor.textProperty().setValue(this.bike.getVendor());
		this.rentPrice.textProperty().setValue(this.bike.getRentPrice());
		this.price.textProperty().setValue(this.bike.getPrice());
		this.bodyNumber.textProperty().setValue(this.bike.getBodyNumber());
		this.spec.textProperty().setValue(this.bike.getSpec());

		// Load image
		Image img = new Image(bike.getImage(), true);
		this.bikeImage.imageProperty().setValue(img);

		// Initialize color label
		this.status.textProperty().setValue("");
		Status status = Status.valueOf(bike.getStatus());
		switch (status) {
			case INRENT:
				this.status.setStyle("-fx-background-color: darkred;");
				break;
			case INREPAIR:
				this.status.setStyle("-fx-background-color: yellow;");
				break;
			case SOLD:
				this.status.setStyle("-fx-background-color: black;");
				break;
			case READY:
				this.status.setStyle("-fx-background-color: darkgreen;");
				break;
		}

		// Initialize category list
		this.category.textProperty().setValue(this.bike.getCategoriesAsString());
	}
}
