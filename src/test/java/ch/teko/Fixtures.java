package ch.teko;

import ch.teko.vo.*;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class Fixtures {

    public static Bike getBike() {
        return Fixtures.getBikeWithNameAndVendor("Paula", "Wheeler");
    }

    public static Bike getBikeWithNameAndVendor(String name, String vendorName) {
        Vendor vendor = new Vendor(vendorName);
        List<BikeCategory> bikeCategoryList = new ArrayList<>();

        return new Bike(
                1,
                name,
                "",
                "",
                0.0,
                0.0,
                "",
                "",
                Status.READY,
                vendor,
                bikeCategoryList
        );
    }

    public static User getUser() {
        return Fixtures.getUser("David", "Weber");
    }

    public static User getUser(String firstName, String lastName) {
        BikeCategory bikeCategory = new BikeCategory("Mountainbike");
        Date date = new Date();

        return new User(
                1,
                firstName,
                lastName,
                "",
                date,
                "",
                bikeCategory
        );

    }

}
