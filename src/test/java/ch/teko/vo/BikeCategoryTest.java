package ch.teko.vo;

import org.junit.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;

public class BikeCategoryTest {

    @Test
    public void getBikeCategoryAsString() {
        String name = "Mountainbike";

        BikeCategory testee = new BikeCategory(
                name
        );

        String result = testee.getName();
        assertThat(result, is(equalTo(name)));
    }

}
