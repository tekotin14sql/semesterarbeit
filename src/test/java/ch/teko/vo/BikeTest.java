package ch.teko.vo;

import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;

public class BikeTest {

    @Test
    public void getBikeNameAsString() {
        String name = "Paula";
        Vendor vendor = new Vendor("Wheeler");
        List<BikeCategory> bikeCategoryList = new ArrayList<>();

        Bike testee = new Bike(
                1,
                name,
                "",
                "",
                0.0,
                0.0,
                "",
                "",
                Status.INRENT,
                vendor,
                bikeCategoryList
        );

        String result = testee.getFullname();
        assertThat(result, is(equalTo(vendor.getName() + " " + name)));
    }

}
