package ch.teko.vo;

import org.junit.Test;

import java.util.Date;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;

public class UserTest {

    @Test
    public void getUserNameAsString() {
        String fristName = "Paula";
        String lastName = "Müller";

        BikeCategory bikeCategory = new BikeCategory("Mountainbike");
        Date date = new Date();

        User testee = new User(
                1,
                fristName,
                lastName,
                "",
                date,
                "",
                bikeCategory
        );

        String result = testee.getFullname();
        assertThat(result, is(equalTo(fristName + " " + lastName)));
    }

}
