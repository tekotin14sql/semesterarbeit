package ch.teko.vo;

import org.junit.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;

public class VendorTest {

    @Test
    public void getVendorAsString() {
        String name = "Wheeler";

        Vendor testee = new Vendor(
                name
        );

        String result = testee.getName();
        assertThat(result, is(equalTo(name)));
    }

}
