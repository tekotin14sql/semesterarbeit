package ch.teko.dao;

import org.junit.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;

public class PropertyFileTranslationHelperDAOTest {

    @Test
    public void getTranslationWhenGetTranslationThenGetStringOfTranslation() {
        // Arrange
        TranslationHelperDAO testee = new PropertyFileTranslationHelperDAO();
        // Act
        String result = testee.getTranslation("bike.search.name");
        // Assert
        assertThat(result, is(equalTo("Name")));
    }

}
