package ch.teko.dao;

import ch.teko.Fixtures;
import ch.teko.vo.Bike;
import ch.teko.vo.Status;
import ch.teko.vo.User;
import org.junit.Test;

import java.util.Date;
import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;

public class MySqlBikeDAOTest {

    @Test
    public void getAllBikesWhenGetAllBikesThenGetListOfBikes() {
        // Arrange
        BikeDAO testee = new MySqlBikeDAO();
        // Act
        List<Bike> result = testee.getAllBikes();
        // Assert
        assertThat(result, hasSize(greaterThan(0)));
    }

    @Test
    public void getAllBikesWhenGetAllBikesWithFilterThenGetListOfBikes() {
        // Arrange
        BikeDAO testee = new MySqlBikeDAO();
        // Act
        List<Bike> result = testee.getAllBikes(true);
        // Assert
        assertThat(result, hasSize(greaterThan(0)));
    }

    @Test
    public void getOneBikeWhenGetBikeByIdThenGetOneBike() {
        // Arrange
        BikeDAO testee = new MySqlBikeDAO();
        // Act
        Bike bike = Fixtures.getBike();
        Bike result = testee.getBikeById(bike);
        // Assert
        assertThat(result, is(notNullValue()));
    }

    @Test
    public void updateOneBikeWhenSellBikeThenGetUpdatedBike() {
        // Arrange
        BikeDAO testee = new MySqlBikeDAO();
        // Act
        Bike bike = Fixtures.getBike();
        Bike result = testee.sellBike(bike);
        // Assert
        assertThat(result, is(notNullValue()));
        assertThat(result.getStatus(), is(Status.SOLD.toString()));
    }

    @Test
    public void updateOneBikeWhenRepairBikeThenGetUpdatedBike() {
        // Arrange
        BikeDAO testee = new MySqlBikeDAO();
        // Act
        Bike bike = Fixtures.getBike();
        Bike result = testee.repairBike(bike);
        // Assert
        assertThat(result, is(notNullValue()));
        assertThat(result.getStatus(), is(Status.INREPAIR.toString()));
    }

    @Test
    public void updateOneBikeWhenReadyBikeThenGetUpdatedBike() {
        // Arrange
        BikeDAO testee = new MySqlBikeDAO();
        // Act
        Bike bike = Fixtures.getBike();
        Bike result = testee.readyBike(bike);
        // Assert
        assertThat(result, is(notNullValue()));
        assertThat(result.getStatus(), is(Status.READY.toString()));
    }

    @Test
    public void updateOneBikeWhenRentBikeThenGetUpdatedBike() {
        // Arrange
        BikeDAO testee = new MySqlBikeDAO();
        Bike bike = Fixtures.getBike();
        User user = Fixtures.getUser();
        // Act
        Bike result = testee.rentBike(bike, user, new Date(), new Date());
        // Assert
        assertThat(result, is(notNullValue()));
        assertThat(result.getStatus(), is(Status.INRENT.toString()));
    }

}
