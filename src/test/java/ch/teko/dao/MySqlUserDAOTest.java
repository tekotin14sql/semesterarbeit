package ch.teko.dao;

import ch.teko.vo.User;
import org.junit.Test;

import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;

public class MySqlUserDAOTest {

    @Test
    public void getAllUsersWhenGetAllUsersThenGetListOfUsers() {
        // Arrange
        UserDAO testee = new MySqlUserDAO();
        // Act
        List<User> result = testee.getAllUsers();
        // Assert
        assertThat(result, hasSize(greaterThan(0)));
    }

    @Test
    public void getOneUserWhenGetUserByIdThenGetOneUser() {
        // Arrange
        UserDAO testee = new MySqlUserDAO();
        // Act
        User result = testee.getUserById(1);
        // Assert
        assertThat(result, is(notNullValue()));
    }

}
